variable "project_credentials" {
  type        = string
  description = "project credentials"
  sensitive   = true
}

variable "project_id" {
  type        = string
  description = "gcp project id"
}

variable "project_region" {
  type        = string
  description = "gcp project region"
}

variable "project_zone" {
  type        = string
  description = "gcp project zone"
}
