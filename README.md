# Registry
- url: https://registry.terraform.io/
- Provider -> GCP -> Document
- url: https://github.com/orgs/terraform-google-modules/repositories
- Module -> Google

# Terraform Variable
- terraform.tfvars
- *.auto.tfvars
- export TF_VAR_variable123 = abc
- terraform plan -var abc=123
- terraform plan -var-file abc.tfvars

# VSCode
* 安裝 hashicorp.terraform
* 喜好設定 -> 鍵盤快速鍵 -> editor.action.triggerSuggest
* 註解: Ctrl + K, C
* 取消註解: Ctrl + K, U
* 設定格式化: 喜好設定 -> 設定 -> 工作區
* .vscode -> settings.json
```json
{
  "editor.defaultFormatter": "hashicorp.terraform",
  "editor.formatOnSave": true
}
```

# 啟動服務
```
gcloud services list

gcloud services enable artifactregistry.googleapis.com \
  compute.googleapis.com \
  run.googleapis.com \
  sqladmin.googleapis.com \
  networkmanagement \
  vpcaccess.googleapis.com
```

# google_compute_network
```
terraform import google_compute_network.network-name projects/{{project}}/global/networks/{{name}}

terraform import google_compute_network.default projects/golden-sandbox-342310/global/networks/default

```

# google_compute_firewall
```
terraform import google_compute_firewall.rule-name projects/{{project}}/global/firewalls/{{name}}

terraform import google_compute_firewall.default-allow-iap-lb projects/golden-sandbox-342310/global/firewalls/default-allow-iap-lb

terraform import google_compute_firewall.default-allow-iap-ssh projects/golden-sandbox-342310/global/firewalls/default-allow-iap-ssh

terraform import google_compute_firewall.default-allow-vpn projects/golden-sandbox-342310/global/firewalls/default-allow-vpn
```

# google_sql_database_instance
```

terraform import google_sql_database_instance.nweb-mysql projects/golden-sandbox-342310/instances/nweb-mysql

```

# google_sql_user
```
terraform import google_sql_user.users golden-sandbox-342310/nweb-mysql/%/root
```

# google_sql_database
```
terraform import google_sql_database.admin projects/golden-sandbox-342310/instances/nweb-mysql/databases/admin

terraform import google_sql_database.ezpay projects/golden-sandbox-342310/instances/nweb-mysql/databases/ezpay
```

# google_cloud_run_service
```
terraform import google_cloud_run_service.admin-webapi locations/asia-east1/namespaces/golden-sandbox-342310/services/admin-webapi

terraform import google_cloud_run_service.ezpay-web locations/asia-east1/namespaces/golden-sandbox-342310/services/ezpay-web

terraform import google_cloud_run_service.ezpay-paymentapi locations/asia-east1/namespaces/golden-sandbox-342310/services/ezpay-paymentapi

terraform import google_cloud_run_service.ezpay-merchantapi locations/asia-east1/namespaces/golden-sandbox-342310/services/ezpay-merchantapi

terraform import google_cloud_run_service.ezpay-callback locations/asia-east1/namespaces/golden-sandbox-342310/services/ezpay-callback
```

# google_compute_region_network_endpoint_group
```
terraform import google_compute_region_network_endpoint_group.ezpay-8bitpay-neg projects/golden-sandbox-342310/regions/asia-east1/networkEndpointGroups/ezpay-8bitpay-neg

terraform import google_compute_region_network_endpoint_group.ezpay-caibao-neg projects/golden-sandbox-342310/regions/asia-east1/networkEndpointGroups/ezpay-caibao-neg

terraform import google_compute_region_network_endpoint_group.ezpay-callback-neg projects/golden-sandbox-342310/regions/asia-east1/networkEndpointGroups/ezpay-callback-neg

```

# google_compute_backend_service
```
terraform import google_compute_backend_service.ezpay-8bitpay-backend projects/golden-sandbox-342310/global/backendServices/ezpay-8bitpay-backend

terraform import google_compute_backend_service.ezpay-caibao-backend projects/golden-sandbox-342310/global/backendServices/ezpay-caibao-backend

terraform import google_compute_backend_service.ezpay-callback-backend projects/golden-sandbox-342310/global/backendServices/ezpay-callback-backend
```

# google_compute_global_address
```
terraform import google_compute_global_address.admin-web-lb-ip projects/golden-sandbox-342310/global/addresses/admin-web-lb-ip
```

# google_compute_managed_ssl_certificate
```
terraform import google_compute_managed_ssl_certificate.admin-web-cert projects/golden-sandbox-342310/global/sslCertificates/admin-web-cert
```

# google_compute_global_forwarding_rule
```
terraform import google_compute_global_forwarding_rule.admin-web-lb-forwarding-rule-2 projects/golden-sandbox-342310/global/forwardingRules/admin-web-lb-forwarding-rule-2

```

# google_compute_target_https_proxy
```
terraform import google_compute_target_https_proxy.admin-web-lb-target-proxy-2 projects/golden-sandbox-342310/global/targetHttpsProxies/admin-web-lb-target-proxy-2
```

# google_compute_url_map
```
terraform import google_compute_url_map.admin-web-lb projects/golden-sandbox-342310/global/urlMaps/admin-web-lb
```