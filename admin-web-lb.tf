# NEG
resource "google_compute_region_network_endpoint_group" "admin-webapi-cloudrun" {
  name                  = "admin-webapi-cloudrun"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.admin-webapi.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-web-cloudrun" {
  name                  = "ezpay-web-cloudrun"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-web.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-paymentapi-neg" {
  name                  = "ezpay-paymentapi-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-paymentapi.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-8bitpay-neg" {
  name                  = "ezpay-8bitpay-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-8bitpay.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-caibao-neg" {
  name                  = "ezpay-caibao-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-caibao.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-callback-neg" {
  name                  = "ezpay-callback-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-callback.name
  }
}

resource "google_compute_region_network_endpoint_group" "ezpay-merchantapi-neg" {
  name                  = "ezpay-merchantapi-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.project_region
  cloud_run {
    service = google_cloud_run_service.ezpay-merchantapi.name
  }
}

# Backend
resource "google_compute_backend_service" "admin-webapi-backend" {
  name       = "admin-webapi-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.admin-webapi-cloudrun.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-web-backend" {
  name       = "ezpay-web-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-web-cloudrun.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-paymentapi-backend" {
  name       = "ezpay-paymentapi-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-paymentapi-neg.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-merchantapi-backend" {
  name       = "ezpay-merchantapi-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-merchantapi-neg.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-8bitpay-backend" {
  name       = "ezpay-8bitpay-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-8bitpay-neg.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-caibao-backend" {
  name       = "ezpay-caibao-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-caibao-neg.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

resource "google_compute_backend_service" "ezpay-callback-backend" {
  name       = "ezpay-callback-backend"
  enable_cdn = false

  connection_draining_timeout_sec = 10
  timeout_sec                     = 30
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  locality_lb_policy              = "ROUND_ROBIN"

  backend {
    group           = google_compute_region_network_endpoint_group.ezpay-callback-neg.id
    capacity_scaler = 0
    balancing_mode  = "UTILIZATION"
  }
}

# URL Map
resource "google_compute_url_map" "admin-web-lb" {
  name = "admin-web-lb"

  default_service = google_compute_backend_service.ezpay-web-backend.id

  host_rule {
    hosts = [
      "admin.xrszyc.com",
    ]
    path_matcher = "path-matcher-1"
  }

  host_rule {
    hosts = [
      "api.xrszyc.com",
    ]
    path_matcher = "path-matcher-2"
  }

  path_matcher {
    name            = "path-matcher-1"
    default_service = google_compute_backend_service.ezpay-web-backend.id

    path_rule {
      paths = [
        "/api/*",
      ]
      service = google_compute_backend_service.admin-webapi-backend.id
    }

    path_rule {
      paths = [
        "/api/pay/*",
      ]
      service = google_compute_backend_service.ezpay-paymentapi-backend.id
    }
  }

  path_matcher {
    default_service = "https://www.googleapis.com/compute/v1/projects/golden-sandbox-342310/global/backendServices/ezpay-doc-backend"
    name            = "path-matcher-2"

    path_rule {
      paths = [
        "/api/*",
      ]
      service = "https://www.googleapis.com/compute/v1/projects/golden-sandbox-342310/global/backendServices/ezpay-merchantapi-backend"
    }
    path_rule {
      paths = [
        "/callback/*",
      ]
      service = "https://www.googleapis.com/compute/v1/projects/golden-sandbox-342310/global/backendServices/ezpay-callback-backend"
    }
    path_rule {
      paths = [
        "/8bitpay/*",
      ]
      service = "https://www.googleapis.com/compute/v1/projects/golden-sandbox-342310/global/backendServices/ezpay-8bitpay-backend"
    }
    path_rule {
      paths = [
        "/caibao/*",
      ]
      service = "https://www.googleapis.com/compute/v1/projects/golden-sandbox-342310/global/backendServices/ezpay-caibao-backend"
    }
  }
}

# SSL
resource "google_compute_managed_ssl_certificate" "admin-web-cert" {
  name = "admin-web-cert"
  managed {
    domains = [
      "admin.xrszyc.com",
      "api.xrszyc.com"
    ]
  }
}

resource "google_compute_target_https_proxy" "admin-web-lb-target-proxy-2" {
  name    = "admin-web-lb-target-proxy-2"
  url_map = google_compute_url_map.admin-web-lb.id
  ssl_certificates = [
    google_compute_managed_ssl_certificate.admin-web-cert.id
  ]
}

# Load Balancer IP
resource "google_compute_global_address" "admin-web-lb-ip" {
  name         = "admin-web-lb-ip"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}

resource "google_compute_global_forwarding_rule" "admin-web-lb-forwarding-rule-2" {
  name                  = "admin-web-lb-forwarding-rule-2"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  ip_protocol           = "TCP"
  port_range            = "443"
  ip_address            = google_compute_global_address.admin-web-lb-ip.address
  target                = google_compute_target_https_proxy.admin-web-lb-target-proxy-2.id
}
