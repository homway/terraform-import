terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.17.0"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "4.17.0"
    }
  }
}

provider "google" {
  credentials = var.project_credentials
  project     = var.project_id
  region      = var.project_region
  zone        = var.project_zone
}

provider "google-beta" {
  credentials = var.project_credentials
  project     = var.project_id
  region      = var.project_region
  zone        = var.project_zone
}
