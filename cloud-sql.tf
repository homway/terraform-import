resource "google_sql_database_instance" "ezpay-mysql" {
  name             = "ezpay-mysql"
  region           = var.project_region
  database_version = "MYSQL_8_0"
  # depends_on = [
  #   "google_service_networking_connection.private_vpc_connection"
  # ]

  settings {
    tier              = "db-g1-small"
    activation_policy = "ALWAYS"
    disk_autoresize   = true
    disk_size         = 10
    disk_type         = "PD_HDD"
    availability_type = "ZONAL"

    # ip_configuration {
    #   authorized_networks {
    #     name  = "armin-ip"
    #     value = "182.155.160.215/32"
    #   }
    # }

    # backup_configuration {
    #   binary_log_enabled = false
    #   enabled            = false
    #   start_time         = "07:00"
    # }

    maintenance_window {
      day          = 1
      hour         = 3
      update_track = "stable"
    }
  }

  deletion_protection = "true"
}

resource "google_sql_user" "users" {
  name     = "root"
  instance = google_sql_database_instance.ezpay-mysql.name
  host     = "%"
  password = "jDKuDfa9gdpE3JpKhVnA"
}


resource "google_sql_database" "admin" {
  name      = "admin"
  instance  = google_sql_database_instance.ezpay-mysql.name
  charset   = "utf8mb4"
  collation = "utf8mb4_unicode_ci"
}

resource "google_sql_database" "ezpay" {
  name      = "ezpay"
  instance  = google_sql_database_instance.ezpay-mysql.name
  charset   = "utf8mb4"
  collation = "utf8mb4_unicode_ci"
}

