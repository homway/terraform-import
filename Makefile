.PHONY: plan

plan:
	terraform plan

apply:
	terraform apply

neg:
	terraform apply \
		-target="google_compute_region_network_endpoint_group.admin-webapi-cloudrun" \
		-target="google_compute_region_network_endpoint_group.ezpay-web-cloudrun" \
		-target="google_compute_region_network_endpoint_group.ezpay-paymentapi-neg" \
		-target="google_compute_region_network_endpoint_group.ezpay-8bitpay-neg" \
		-target="google_compute_region_network_endpoint_group.ezpay-caibao-neg" \
		-target="google_compute_region_network_endpoint_group.ezpay-callback-neg" \
		-target="google_compute_region_network_endpoint_group.ezpay-merchantapi-neg"

backend:
	terraform apply \
		-target="google_compute_backend_service.admin-webapi-backend" \
		-target="google_compute_backend_service.ezpay-web-backend" \
		-target="google_compute_backend_service.ezpay-paymentapi-backend" \
		-target="google_compute_backend_service.ezpay-8bitpay-backend" \
		-target="google_compute_backend_service.ezpay-caibao-backend" \
		-target="google_compute_backend_service.ezpay-callback-backend" \
		-target="google_compute_backend_service.ezpay-merchantapi-backend"


urlmap:
	terraform apply -target="google_compute_url_map.admin-web-lb"

ssl:
	terraform apply -target="google_compute_managed_ssl_certificate.admin-web-cert"

firewall:
	terraform apply -target="google_compute_firewall.default-allow-iap-lb" \
		-target="google_compute_firewall.default-allow-vpn" \
		-target="google_compute_firewall.default-allow-iap-ssh"

cloudsql:
	terraform apply -target="google_sql_database_instance.ezpay-mysql" \
		-target="google_sql_user.users" \
		-target="google_sql_database.admin" \
		-target="google_sql_database.ezpay" \

cloudrun:
	terraform apply -target="google_cloud_run_service.admin-webapi" \
		-target="google_cloud_run_service.ezpay-8bitpay" \
		-target="google_cloud_run_service.ezpay-caibao" \
		-target="google_cloud_run_service.ezpay-callback" \
		-target="google_cloud_run_service.ezpay-merchantapi" \
		-target="google_cloud_run_service.ezpay-paymentapi" \
		-target="google_cloud_run_service.ezpay-rsaapi" \
		-target="google_cloud_run_service.ezpay-web"

