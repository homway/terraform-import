
resource "google_cloud_run_service" "admin-webapi" {
  name     = "admin-webapi"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/admin-webapi@sha256:f1d57698c88f9896e9e97241550160f047a63c7deebdef9132fe44eae0a0d9bb"

        env {
          name  = "ADMIN_DB_MYSQL_URL"
          value = "172.16.176.8"
        }
        env {
          name  = "PAY_DB_MYSQL_URL"
          value = "172.16.176.8"
        }
        env {
          name  = "SMS_DB_MYSQL_URL"
          value = "172.16.176.8"
        }
        env {
          name = "ADMIN_DB_MYSQL_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "ezpay-mysql-password"
            }
          }
        }
        env {
          name = "PAY_DB_MYSQL_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "ezpay-mysql-password"
            }
          }
        }
        env {
          name = "SMS_DB_MYSQL_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "ezpay-mysql-password"
            }
          }
        }

      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-8bitpay" {
  name     = "ezpay-8bitpay"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.8bitpay:22.06.10"

        env {
          name  = "ELASTIC_APM_ENVIRONMENT"
          value = "dev"
        }
        env {
          name  = "ELASTIC_APM_SERVICE_VERSION"
          value = "0.0.2"
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-caibao" {
  name     = "ezpay-caibao"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.caibao:22.06.2"

        env {
          name  = "ELASTIC_APM_ENVIRONMENT"
          value = "dev"
        }
        env {
          name  = "ELASTIC_APM_SERVICE_VERSION"
          value = "0.0.1"
        }
      }


    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-callback" {
  name     = "ezpay-callback"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.callback:22.05.3"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-merchantapi" {
  name     = "ezpay-merchantapi"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.merchantapi:22.06.35"

        env {
          name  = "TYPEORM_DATABASE"
          value = "ezpay"
        }
        env {
          name  = "TYPEORM_HOST"
          value = "172.16.176.8"
        }
        env {
          name = "TYPEORM_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "ezpay-mysql-password"
            }
          }
        }
        env {
          name  = "TYPEORM_USERNAME"
          value = "root"
        }
        env {
          name  = "ELASTIC_APM_ENVIRONMENT"
          value = "dev"
        }
        env {
          name  = "ELASTIC_APM_SERVICE_VERSION"
          value = "22.05.10"
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-paymentapi" {
  name     = "ezpay-paymentapi"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.paymentapi:22.05.23"

        env {
          name  = "TYPEORM_DATABASE"
          value = "ezpay"
        }
        env {
          name  = "TYPEORM_HOST"
          value = "172.16.176.8"
        }
        env {
          name = "TYPEORM_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "ezpay-mysql-password"
            }
          }
        }
        env {
          name  = "TYPEORM_USERNAME"
          value = "root"
        }
        env {
          name  = "ELASTIC_APM_ENVIRONMENT"
          value = "dev"
        }
        env {
          name  = "ELASTIC_APM_SERVICE_VERSION"
          value = "22.05.17"
        }
      }


    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service" "ezpay-web" {
  name     = "ezpay-web"
  location = var.project_region

  template {
    spec {
      containers {
        image = "asia-east1-docker.pkg.dev/golden-sandbox-342310/ezpay/ezpay.web:22.05.12"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}


