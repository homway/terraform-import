resource "google_compute_firewall" "default-allow-iap-lb" {
  name          = "default-allow-iap-lb"
  network       = "default"
  description   = "allow gcp load balancer health check"
  direction     = "INGRESS"
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  priority      = 1000
  disabled      = false

  allow {
    protocol = "tcp"
    ports    = ["80", "8080"]
  }
}

resource "google_compute_firewall" "default-allow-iap-ssh" {
  name          = "default-allow-iap-ssh"
  network       = "default"
  description   = "allow gcp ssh and mstsc"
  direction     = "INGRESS"
  source_ranges = ["35.235.240.0/20"]
  priority      = 1000
  disabled      = false

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "default-allow-vpn" {
  name          = "default-allow-vpn"
  network       = "default"
  description   = "allow vpn"
  direction     = "INGRESS"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["vpn"]
  priority      = 1000
  disabled      = false

  allow {
    protocol = "udp"
    ports    = ["500", "4500"]
  }
}
